

//--- The sprite object
var spriteObject =
{
    sourceX: 0,
    sourceY: 0,
    sourceWidth: 64,
    sourceHeight: 64,
    x: 0,
    y: 0,
    width: 64,
    height: 64,
    vx: 0,
    vy: 0,

    //pysics
    bounce: -0.7,
    gravity: 0.3,
        
    //Platform game properties   
    isOnGround: undefined,
    jumpForce: -10,

    //getters
    centerX: function()
    {
        return this.x + (this.width / 2);
    },
    centerY: function()
    {
        return this.y + (this.height / 2);
    },
    halfWidth: function()
    {
        return this.width / 2;
    },
    halfHeight: function()
    {
        return this.height / 2;
    }

};





//The canvas and its drawing surface
var canvas = document.querySelector("canvas");
var drawingSurface = canvas.getContext("2d");

var over = document.querySelector("canvas");
var gameOver = over.getContext("2d");

log = document.getElementById("log");
var score = 0;
var lives = 5;



//An array to store the sprites
var sprites = [];

//Create the distantBackground sprite
var distantBackground = Object.create(spriteObject);
distantBackground.sourceY = 64;
distantBackground.sourceWidth = 1190;
distantBackground.sourceHeight = 238;
distantBackground.width = 1190;
distantBackground.height = 238;
distantBackground.x = 0;
distantBackground.y = 0;
sprites.push(distantBackground);

//Create the foreground sprite
var foreground = Object.create(spriteObject);
foreground.sourceY = 302;
foreground.sourceWidth = 1190;
foreground.sourceHeight = 238;
foreground.width = 1190;
foreground.height = 238;
foreground.x = 0;
foreground.y = 0;
sprites.push(foreground);

//Create the gameWorld and camera objects
var gameWorld =
{
    x: 0,
    y: 0,
    width: foreground.width,
    height: foreground.height
};

//The camera has 2 new properties: "vx" and "previousX"
var camera =
{
    x: 0,
    y: 0,
    width: canvas.width,
    height: canvas.height,
    vx: 0,
    vy: 0,
    previousX: 0,

    //The camera's inner scroll boundaries
    rightInnerBoundary: function()
    {
        return this.x + (this.width * 0.75);
    },
    leftInnerBoundary: function()
    {
        return this.x + (this.width * 0.25);
    }
};

//Center the camera over the gameWorld
camera.x = (gameWorld.x + gameWorld.width / 2) - camera.width / 2;
camera.y = (gameWorld.y + gameWorld.height / 2) - camera.height / 2;

//Create the cat sprite and center it
var cat = Object.create(spriteObject);
cat.sourceX=0;
cat.sourceY=0;
cat.x = (gameWorld.x + gameWorld.width / 2) - cat.width / 2;
cat.y = 174; //(gameWorld.y + gameWorld.height / 2) - cat.height / 2;
sprites.push(cat);

//create the point sprite and place it
var point = Object.create(spriteObject);
point.sourceY = 0;
point.sourceX = 64;
point.sourceHeight= 37;
point.sourceWidth = 80;
point.height = 37;
point.width= 80;
point.x= 50;
point.y=200;
sprites.push(point);

var enemy = Object.create(spriteObject);
enemy.sourceY=0;
enemy.sourceX = 144;
enemy.sourceHeight = 96;
enemy.sourceWidth= 64;
enemy.height= 96;
enemy.width= 64;
enemy.x= 1126;
enemy.y= 142;
sprites.push(enemy);

//Load the image
var image = new Image();
function clickHandler() {
    update();
}
//image.addEventListener("load", loadHandler, false);
image.src = "parallaxScrollingTileSheet.png";

//Arrow key codes
var RIGHT = 39;
var LEFT = 37;
var SPACE = 49;

//Directions
var moveUp = false;
var moveRight = false;
var moveLeft = false;

//Add keyboard listeners
window.addEventListener("keydown", function(event)
{
    switch(event.keyCode)
    {
        case LEFT:
            console.log("LEFT true");
            moveLeft = true;
            break;

        case RIGHT:
            console.log("RIGHT true");
            moveRight = true;
            break;

        case SPACE:
            console.log("SPACE true");
            moveUp = true;
            break;
    }
}, false);

window.addEventListener("keyup", function(event)
{
    switch(event.keyCode)
    {
        case LEFT:
            console.log("LEFT false");
            moveLeft = false;
            break;

        case RIGHT:
            console.log("RIGHT false");
            moveRight = false;
            break;
        
        case SPACE:
            console.log("SPACE false");
            moveUp = false;
            break;
    }
}, false);

function loadHandler()
{
    
    update();
}

function update()
{
    //The animation loop
    requestAnimationFrame(update, canvas);

    //Left
    if(moveLeft && !moveRight &&!moveUp){
        console.log("moveLeft");
        cat.vx = -5;
    }
    //Right
    if(moveRight && !moveLeft &&!moveUp){
        console.log("moveRight");
        cat.vx = 5;
    }

    //jump
    if(moveUp && moveRight){
        cat.gravity = 0;
        cat.vy = -10;
        cat.vx = 5
        cat.isOnGround = false;
    }
    if(moveUp && moveLeft){
        cat.gravity = 0;
        cat.vy = -10;
        cat.vx = -5
        cat.isOnGround = false;
    }

    if(!moveUp){
        cat.gravity = 5;
        cat.vy +=0.5;
    }
    
    //Set the cat's velocity to zero if none of the keys are being pressed
    if(!moveUp && !moveLeft && !moveRight){
        cat.gravity = 5;
        console.log("Do nothing");
        cat.vy = 0;
        cat.vx = 0;
    }


    cat.vy += cat.gravity

    //Move the cat and keep it inside the gameWorld boundaries
    cat.x = Math.max(0, Math.min(cat.x + cat.vx, gameWorld.width - cat.width));
    console.log("cat: " +cat.x)
    cat.y = Math.max(0, Math.min(cat.y + cat.vy, gameWorld.width - cat.width));
    console.log("cat vx: " + cat.vy)
    console.log("cat: " + cat.y)

    if(cat.y + cat.height > canvas.height)
    {
        cat.vy *= cat.bounce;
        cat.y = canvas.height - cat.height;
    }


    //Scroll the camera
    if(cat.x < camera.leftInnerBoundary())
    {
        camera.x = Math.floor(cat.x - (camera.width * 0.25));
    }
    if(cat.x + cat.width > camera.rightInnerBoundary())
    {
        camera.x = Math.floor(cat.x + cat.width - (camera.width * 0.75));
    }

    //The camera's world boundaries
    if(camera.x < gameWorld.x)
    {
        camera.x = gameWorld.x;
    }
    if(camera.x + camera.width > gameWorld.x + gameWorld.width)
    {
        camera.x = gameWorld.x + gameWorld.width - camera.width;
    }

    //Figure out the camera's velocity by subtracting its position in the
    //previous frame from its position in this frame
    camera.vx = camera.x - camera.previousX;

    //Move the distantBackground at half the speed of the camera
    distantBackground.x += camera.vx / 2;

    //Capture the camera's current x position so we can use it as the
    //previousX value in the next frame
    camera.previousX = camera.x;

    if(hitTestRectangle(cat, point))
    {
        //Collision!
        score += 5;
         point.x = Math.floor(Math.random() * (foreground.width - 64));

    }

    if(hitTestRectangle(cat, enemy))
    {
        //Collision!
        lives--;
        enemy.x = Math.floor(Math.random() * (foreground.width - 64));

    }


    render();
}

function hitTestRectangle(r1, r2)
{
    //A variable to determine whether there's a collision
    var hit = false;

    //Calculate the distance vector
    var vx = r1.centerX() - r2.centerX();
    var vy = r1.centerY() - r2.centerY();

    //Figure out the combined half-widths and half-heights
    var combinedHalfWidths = r1.halfWidth() + r2.halfWidth();
    var combinedHalfHeights = r1.halfHeight() + r2.halfHeight();

    //Check for a collision on the x axis
    if(Math.abs(vx) < combinedHalfWidths)
    {
        //A collision might be occuring. Check for a collision on the y axis
        if(Math.abs(vy) < combinedHalfHeights)
        {
            //There's definitely a collision happening
            hit = true;
        }
        else
        {
            //There's no collision on the y axis
            hit = false;
        }
    }
    else
    {
        //There's no collision on the x axis
        hit = false;
    }

    return hit;
}


function render(event)
{

    var run= cat.x - enemy.x;

    enemy.x += run * 0.01;
    drawingSurface.clearRect(0, 0, canvas.width, canvas.height);

    function logfile(){
        log.value ="SCORE: " +score +"\nLIVES: " + lives;

     }
    logfile();

    drawingSurface.save();

    //Move the drawing surface so that it's positioned relative to the camera
    drawingSurface.translate(-camera.x, -camera.y);

    //Loop through all the sprites and use their properties to display them
    if(lives != 0)
    {
    if(sprites.length !== 0)
    {
        for(var i = 0; i < sprites.length; i++)
        {
            var sprite = sprites[i];
            drawingSurface.drawImage
                    (
                            image,
                            sprite.sourceX, sprite.sourceY,
                            sprite.sourceWidth, sprite.sourceHeight,
                            Math.floor(sprite.x), Math.floor(sprite.y),
                            sprite.width, sprite.height
                    );
        }

    }
    }

     drawingSurface.restore();
}
